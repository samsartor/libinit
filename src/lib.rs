/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use futures::pin_mut;
use futures::prelude::*;
use std::future::Future;
use std::path::Path;
use tokio::prelude::*;

pub mod debug;

#[macro_export]
macro_rules! debug {
    ($($x:tt)*) => {
        $crate::debug::debug(format_args!($($x)*))
    };
}

fn mount_dev() -> Result<(), nix::Error> {
    use nix::mount;
    use std::ffi::CStr;

    mount::mount(
        Option::<&[u8]>::None,
        CStr::from_bytes_with_nul(b"/dev\0").unwrap(),
        Some(CStr::from_bytes_with_nul(b"devtmpfs\0").unwrap()),
        mount::MsFlags::empty(),
        Option::<&[u8]>::None,
    )
}

pub fn initialize<O>(config: impl Future<Output = O>) -> O {
    println!("Welcome libinit!");

    // Setup /dev directory. We need to actually block on this since tokio will
    // crash if /dev/urandom is not available (salt is needed for HashMaps).
    mount_dev().expect("could not mount devtmpfs");

    // Start actual app
    let mut rt = tokio::runtime::Runtime::new().expect("could not start tokio");
    rt.block_on(config)
}

#[derive(zerocopy::AsBytes, zerocopy::FromBytes, Copy, Clone, Default, Debug)]
#[repr(C)]
pub struct InputEvent {
    pub time: [u8; 16], // libc::timeval
    pub ty: u16,
    pub code: u16,
    pub value: u32,
}

pub async fn fmt_tree(path: impl AsRef<Path>) -> Result<String, std::io::Error> {
    use std::fmt::Write;
    use std::iter::repeat;
    use tokio::fs::{metadata, read_dir};

    let mut output = String::new();
    let mut stack = vec![(0, path.as_ref().to_owned())];
    while let Some((depth, path)) = stack.pop() {
        output.extend(repeat("| ").take(depth));
        // TODO: print out device info using ioctl
        writeln!(&mut output, "{}", path.display()).expect("could not fmt path");
        let meta = metadata(&path).await?;
        if meta.is_dir() {
            let children = read_dir(&path).await?;
            pin_mut!(children);
            while let Some(child) = children.next().await {
                stack.push((depth + 1, child?.path()));
            }
        }
    }

    Ok(output)
}

// TODO: Actually fix this because it doesn't seem to work
pub fn input_events() -> impl Stream<Item = InputEvent> {
    use async_stream::stream;
    use tokio::fs::File;
    use zerocopy::AsBytes;

    stream! {
        // TODO: this device doesn't exist
        let mut events = match File::open("/dev/input/event0").await {
            Ok(e) => e,
            Err(_) => return,
        };
        let mut buffer = InputEvent::default();
        loop {
            match events.read_exact(buffer.as_bytes_mut()).await {
                Ok(_) => (),
                Err(_) => return,
            }
            yield buffer;
        }
    }
}

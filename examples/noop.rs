/* Any copyright is dedicated to the Public Domain.
https://creativecommons.org/publicdomain/zero/1.0/ */

use libinit::initialize;

fn main() -> () {
    initialize(async { () })
}
